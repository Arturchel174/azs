import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Button,
} from 'react-native';



export default class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TextInputName: '',
            TextInputPass: '',
        };
    }

    CheckTextInput = () => {
        //Handler for the Submit onPress
        if (this.state.TextInputName != '') {
            if (this.state.TextInputPass != '') {
                this.props.navigation.navigate('index',{
                    name: this.state.TextInputName,
                } );

            } else {
                alert('pass');
            }
        } else {
            alert('name');
        }
    };

    render() {
        return (
            <View style={styles.MainContainer}>
                <TextInput
                    placeholder="login"
                    onChangeText={TextInputName => this.setState({ TextInputName })}
                    underlineColorAndroid="transparent"
                    style={styles.TextInput}
                />
                <TextInput
                    placeholder="password"
                    secureTextEntry={true}
                    onChangeText={TextInputPass => this.setState({ TextInputPass })}
                    underlineColorAndroid="transparent"
                    style={styles.TextInput}
                />
                <View style={{ marginTop: 15 }}>
                    <Button
                        title="Отправить"
                        onPress={this.CheckTextInput}
                        color="#606070"
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        margin: 35,
    },
    TextInput: {
        width: '100%',
        height: 40,
        paddingLeft: 5,
        borderWidth: 1,
        marginTop: 15,
        borderColor: '#606070',
    },
});