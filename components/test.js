import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class test extends Component {

    render() {
        const { navigation } = this.props;
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>Hello, {navigation.getParam('name')}</Text>
            </View>
        );
    }
}