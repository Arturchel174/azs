import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ScreenOne from '../screens/ScreenOne';
import index from '../components/test';

import BottomTabNavigator from './BottomTabNavigator';


const AuthStack = createStackNavigator(
    {
        Main: BottomTabNavigator,
        index

    },
    {
        headerMode: "none"
    },

);


export default createAppContainer(AuthStack
);
