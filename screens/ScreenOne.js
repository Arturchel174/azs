import React from 'react';
import { View, StyleSheet } from 'react-native';

// pull in the ScreenName component from ScreenName.js
import ScreenName from '../components/ScreenName.js'
import SignIn from "../components/SignIn";

export default class ScreenOne extends React.Component {

  // we won't need to configure navigationOptions just yet
  static navigationOptions = {

  };

  render() {
    return (
        //<SignIn name = {'dsfdsf'}/>
        <SignIn navigation = {this.props.navigation}/>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 35,
  },
});
